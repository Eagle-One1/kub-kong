#!/bin/sh
deploy_nginx () {
    helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
    helm repo update


    ingress="$(kubectl get pods |grep ingress-nginx-controller)"
    if [[ "$ingress" =~ .*ingress-nginx-controller.* ]] ; 
    then 
        helm upgrade ingress-nginx ingress-nginx/ingress-nginx
    else
        helm install ingress-nginx ingress-nginx/ingress-nginx
    fi

}

deploy_whoami() {
    helm repo add cowboysysop https://cowboysysop.github.io/charts/
    who="$(helm list |grep who)"
    if [[ "$who" =~ .*whoami.* ]] ; 
    then 
        helm upgrade whoami cowboysysop/whoami -f values.yml
    else
        helm install whoami cowboysysop/whoami -f values.yml
        echo "Strings are not equal."
    fi
}

deploy_kong () {
    helm repo add kong https://charts.konghq.com
    helm repo update
    helm install kong/kong --generate-name --set ingressController.installCRDs=false
}

main() {
    deploy_kong
#    deploy_nginx
    deploy_whoami
}

main
# POD_NAMESPACE=ingress-nginx

#POD_NAME=$(kubectl get pods -n $POD_NAMESPACE -l app.kubernetes.io/name=ingress-nginx --field-selector=status.phase=Running -o jsonpath='{.items[0].metadata.name}')

#kubectl exec -it $POD_NAME -n $POD_NAMESPACE -- /nginx-ingress-controller --version
